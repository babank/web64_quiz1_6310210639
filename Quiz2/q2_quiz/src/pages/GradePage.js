
import Grade from "../components/Grade";
import { useState } from "react";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from "@mui/material";

function GradePage() {

    const [ name, setName ] = useState("");
    const [ pointResult, setPointResult ] = useState("");
    const [ tranResult, setTranResult ]= useState("");
    
    function ResultGrade(){
        let p = parseFloat(pointResult);
        setPointResult(p);

        if ((p>=80)&&(p<=100)) {    
            setTranResult(" A ");
         }
          else if ((p>=75)&&(p<=79)) {    
            setTranResult(" B+ ");
         }
          else if ((p>=70)&&(p<=74)) {       
            setTranResult(" B ");
         }
          else if ((p>=65)&&(p<=69)) {
            setTranResult(" C+ ");
         }
          else if ((p>=60)&&(p<=64)) {    
            setTranResult(" C ");
         }
          else if ((p>=55)&&(p=59)) {            
            setTranResult(" D+ ");
         }
          else if ((p>=35)&&(p<=54)) {       
            setTranResult(" D "); 
         }
          else if (p<35) {       
            setTranResult(" E ");
         }   
    }


    return (
        <Container maxWidth='lg'>   
        <Grid container spacing={2} sx={{ marginTop : "10px" }}>
        <Grid item xs={12}>
            <Typography variant="h5"> 
                เว็บคำนวณเกรด
                </Typography>
                </Grid>
                <Grid item xs={8}>
                <Box sx={{ textAlign : 'center' }}>
                <hr />
                ชื่อคุณ : <input type="text" 
                                value={name} 
                                onChange={ (e) => { setName(e.target.value);  }} /> <br />

                คะแนน: <input type="text" 
                                value={pointResult}
                                onChange={ (e) => { setPointResult(e.target.value);  }} /> <br />
                <br />

                <Button variant="contained" onClick={ () =>{ResultGrade()}} > คำนวณเกรด </Button>
                </Box>
                </Grid>
            <Grid item xs={4}>
                { pointResult !=0 &&
                    <div>
                        <br />
                         นี่คือเกรดที่คุณจะได้   
                        <Grade
                        name={ name }
                        point={pointResult}
                        result = { tranResult }
                />
                
                </div>
                }
                </Grid>
            </Grid>
        </Container>
    );
}

export default GradePage;


