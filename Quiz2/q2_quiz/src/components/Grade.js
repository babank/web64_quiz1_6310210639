
function Grade (props) {

    return (
    <div>
        <h4> ชื่อ: {props.name} </h4>
        <h4>คะแนน: {props.point} </h4>
        <h4>เกรด: {props.result} </h4>
    </div>
    
    );
}

export default Grade;