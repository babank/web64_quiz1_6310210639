
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUS (props) {
   
    return (
        <Box sx={{width:"60%"}}>
        <Paper elevation={5} >      
            <h2> จัดทำโดย: { props.name }</h2>
            <h2> อายุ: { props.years }</h2>
            <h2> รหัสนักศึกษา: { props.id } </h2>
        </Paper>
        </Box>
    );
}

export default AboutUS;